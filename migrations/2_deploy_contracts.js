var SimpleStorage = artifacts.require("./SimpleStorage.sol");
var ClipsNGo = artifacts.require("./ClipsNGo.sol");

module.exports = function(deployer) {
    deployer.deploy(SimpleStorage);
    deployer.deploy(ClipsNGo);
};