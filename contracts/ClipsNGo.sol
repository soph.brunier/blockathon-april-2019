pragma solidity >=0.4.22 <0.6.0;

contract ClipsNGo {

    address public owner_cng;
    uint public tmp;
    uint public tmp2;
    mapping(uint => Item) public items;
    mapping(uint => mapping(uint => uint256)) public date_transac;
    mapping(uint => mapping(uint256 => bool)) public etat_transac;

    struct Item{
        address owner;
        uint last_transition_key;
    }  

    constructor () public
    {
       owner_cng = msg.sender;
    }
    
    function createItem(uint _id_nfc) public returns (bool ok) {
        require(owner_cng == msg.sender);
        uint256 ts_key = now;
        uint transition_key = 0;
        
        items[_id_nfc] = Item(address(0x0), transition_key);
        date_transac[_id_nfc][transition_key] = ts_key;
        etat_transac[_id_nfc][ts_key] = false;

        return true;
    }
   
    function transfer(uint _id_nfc, address _to) public{
        require(items[_id_nfc].owner == address(0x0));
        items[_id_nfc].owner = _to;
    }
    
    function reset(uint _id_nfc) public{
        require(items[_id_nfc].owner == msg.sender || owner_cng == msg.sender);
        items[_id_nfc].owner = address(0x0);
    }
    
   
    function startUse(uint _id_nfc, uint256 ts_key) public returns (uint256 ts_debut){
        require(items[_id_nfc].owner == msg.sender);
        //tmp = msg.sender;
        // uint256 ts_key = now;
        uint new_transition_key = items[_id_nfc].last_transition_key + 1;
        
        items[_id_nfc].last_transition_key = new_transition_key;
        
        date_transac[_id_nfc][new_transition_key] = ts_key;
        etat_transac[_id_nfc][ts_key] = true;
        
        return ts_key;
    }
    
    
    function endUse(uint _id_nfc, uint256 ts_key) public returns (uint256 ts_fin){
        require(items[_id_nfc].owner == msg.sender);
        // uint256 ts_key = now;
        uint new_transition_key = items[_id_nfc].last_transition_key + 1;
        
        items[_id_nfc].last_transition_key = new_transition_key;
        
        date_transac[_id_nfc][new_transition_key] = ts_key;
        etat_transac[_id_nfc][ts_key] = false;
        
        return ts_key;
    }
    
    function claim(uint _id_nfc, uint ts_claim) public returns (bool claimOk) {
        uint last_transition_key = items[_id_nfc].last_transition_key;
        uint date_transac_key = date_transac[_id_nfc][last_transition_key];
        
        if (ts_claim <= date_transac[_id_nfc][0]) {
            revert();
        }

        for (uint i = 1; i <= last_transition_key; i++) {
            if(ts_claim <= date_transac[_id_nfc][i]){
                date_transac_key = date_transac[_id_nfc][i - 1];
                break;
            }
        }
        tmp = date_transac_key;
        if(! etat_transac[_id_nfc][date_transac_key]) {
            revert();
        }
    }
}
