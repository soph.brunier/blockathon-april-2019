import React, { Component } from "react";
import ClipsNGoContract from "./contracts/ClipsNGo.json";
import getWeb3 from "./getWeb3";

import "./App.css";

class App extends Component {
  state = { storageValue: 0, web3: null, accounts: null, contract: null };
  startTimestamp = 1573730531; //  14/11/2019 à 11:22:11	
  endTimestamp = 1573734131; //  14/11/2019 à 12:22:11	
  falseTimestamp = 1573726931; //  14/11/2019 à 10:22:11	
  trueTimestamp = 1573731011; //  14/11/2019 à 11:30:11	

  componentDidMount = async () => {
    try {
      // Get network provider and web3 instance.
      const web3 = await getWeb3();

      // Use web3 to get the user's accounts.
      const accounts = await web3.eth.getAccounts();

      // Get the contract instance.
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = ClipsNGoContract.networks[networkId];
      const instance = new web3.eth.Contract(
        ClipsNGoContract.abi,
        deployedNetwork && deployedNetwork.address
      );

      // Set web3, accounts, and contract to the state, and then proceed with an
      // example of interacting with the contract's methods.
      this.setState({ web3, accounts, contract: instance }, this.runExample);
    } catch (error) {
      // Catch any errors for any of the above operations.
      alert(
        `Failed to load web3, accounts, or contract. Check console for details.`
      );
      console.error(error);
    }
  };

  handleClick = async () => {
   
    const { contract, accounts } = this.state;

    // Stores a given value, Add the helmet N°255 in the SmartContract.
    const result = await contract.methods.createItem(255).send({from: accounts[0]});

    console.log(result);
    
    // Get the value from the contract to prove it worked.
    // const response = await contract.methods.get().call();

    // Update state with the result.
    // this.setState({ storageValue: result });
  };

  buy = async () => {
    const { contract, accounts } = this.state;

    // Stores a given value, Add the helmet N°255 in the SmartContract.
    const result = await contract.methods.transfer(255, accounts[0]).send({from: accounts[0]});
    console.log(result);
  };

  start = async () => {
    const { contract, accounts } = this.state;

    // Stores a given value, Add the helmet N°255 in the SmartContract.
    const result = await contract.methods.startUse(255, this.startTimestamp).send({from: accounts[0]});
    console.log(result);
  };

  end = async () => {
    const { contract, accounts } = this.state;

    // Stores a given value, Add the helmet N°255 in the SmartContract.
    const result = await contract.methods.endUse(255, this.endTimestamp).send({from: accounts[0]});
    console.log(result);
  };

  trueClaim = async () => {
    const { contract, accounts } = this.state;

    // Stores a given value, Add the helmet N°255 in the SmartContract.
    const result = await contract.methods.claim(255, this.trueTimestamp).send({from: accounts[0]});
    console.log(result);
  };

  falseClaim = async () => {
    const { contract, accounts } = this.state;

    // Stores a given value, Add the helmet N°255 in the SmartContract.
    const result = await contract.methods.claim(255, this.falseTimestamp).send({from: accounts[0]});
    console.log(result);
  };


  handleChange = async() => {
      const { contract, accounts } = this.state;

      // Transfer the helmet to the user (2nd PK in ganache)
      await contract.methods.transfer(255, accounts[0]).send({from: accounts[0]});
  };

  render() {
    if (!this.state.web3) {
      return <div> Loading Web3, accounts, and contract... </div>;
    }
    return (
      <div className="App">
        <h1> Clips 'N' Go! </h1>
        

        <h2> Smart Contract Example </h2>{" "}
        
        
        <button onClick={ this.handleClick }> Submit </button>{" "}
        <button onClick={ this.buy }> Buy helmet </button>
        <button onClick={ this.start }> Start </button>
        <button onClick={ this.end }> End </button>
        <button onClick={ this.falseClaim }> False Claim </button>
        <button onClick={ this.trueClaim }> True Claim </button>
      </div>
    );
  }
}

export default App;
